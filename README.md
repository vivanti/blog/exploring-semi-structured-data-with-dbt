# Exploring Data with dbt

This code accompanies a series of blog posts, to be published
soon, on the use of [data build tool (dbt)][1] to explore JSON
data sets in their native format.

Included is a a Docker Compose recipe for spinning up a local
PostgreSQL relational database cluster, and some glue code and
Perl scripts to load the raw JSON into landing tables specifically
designed for the blog posts starting point.

To start the compose'd environment, normal tooling works:

    $ docker-compose up

This takes care of both standing up the database and loading the
data.  Persistent data (the files underneath PostgreSQL itself)
are stored in the `./_` directory, in the root of this repository.
Those are excluded from version control, explicitly.

Also included is the final set of dbt models and tests, as
developed throughout the blog series.  These can be found in the
`./dbt/` folder.  You are, however, encouraged to follow along
with the original blog series and build up these models by
yourself.

[1]: https://vivanti.com/2022/07/21/exploring-data-with-dbt-part-1-landing/
