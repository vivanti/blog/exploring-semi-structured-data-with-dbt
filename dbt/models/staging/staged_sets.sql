select distinct
  card->>'set'                  as code,
  card->>'set_name'             as name,
  card->>'set_type'             as type,
  (card->>'released_at')::date  as release_date
from
  {{ ref('raw_cards') }}
where
  card->>'set' not in (
    'med',  -- 3 Mythic Edition sets treated as one
    'pres', -- Rolling "catch-all" set for presales
    'iko',  -- Zilorth was re-released later
    'ala'   -- Rafiq was re-released later
  )

union

select
  card->>'set'                     as code,
  card->>'set_name'                as name,
  card->>'set_type'                as type,
  min(card->>'released_at')::date  as release_date
from
  {{ ref('raw_cards') }}
where
  card->>'set' in (
    'med',  -- 3 Mythic Edition sets treated as one
    'pres', -- Rolling "catch-all" set for presales
    'iko',  -- Zilorth was re-released later
    'ala'   -- Rafiq was re-released later
  )
group by 1, 2, 3
