with cards as (
  select filename, sha1sum, loaded_at,
         jsonb_array_elements(data->'data')
           as card
    from {{ ref('scryfall_data') }}
)
select card->>'id'        as print_id,
       card->>'oracle_id' as oracle_id,
       card->>'name'      as name,

       filename  as source_file_name,
       sha1sum   as source_file_sha1sum,
       loaded_at as source_file_loaded_at,
       card
  from cards
