with duplicates as (
  select code
    from {{ ref('staged_sets') }}
   group by 1 having count(*) &gt; 1
)
select code, name, type, release_date
  from {{ ref('staged_sets') }}
 where code in (select code from duplicates)
