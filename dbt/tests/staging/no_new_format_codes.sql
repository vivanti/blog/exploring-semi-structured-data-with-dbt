with
found_formats as (
  select distinct
    jsonb_object_keys(card->'legalities') as format
  from
    {{ ref('raw_cards') }}
)

select format
  from found_formats
 where format not in (
   {% for format in var('known_formats') %}
   '{{ format }}'
   {% if not loop.last %},{% endif %}
   {% endfor %}
 )
